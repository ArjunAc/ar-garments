import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponentComponent } from './home-component/home-component.component';
import { ProductComponentComponent } from './product-component/product-component.component';
import { AboutUsComponentComponent } from './about-us-component/about-us-component.component';
import { TestimonialComponentComponent } from './testimonial-component/testimonial-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';

const routes: Routes = [
  { path: 'homePage', component: HomeComponentComponent },
  { path: 'aboutUs', component: AboutUsComponentComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    HomeComponentComponent,
    ProductComponentComponent,
    AboutUsComponentComponent,
    TestimonialComponentComponent,
    FooterComponentComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    NgbModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
